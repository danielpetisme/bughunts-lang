// Generated from C:\WORKSPACE\bughunts-lang/antlr4/BugHunts.g4 by ANTLR 4.5
// jshint ignore: start
var antlr4 = require('antlr4/index');

// This class defines a complete listener for a parse tree produced by BugHuntsParser.
function BugHuntsListener() {
	antlr4.tree.ParseTreeListener.call(this);
	return this;
}

BugHuntsListener.prototype = Object.create(antlr4.tree.ParseTreeListener.prototype);
BugHuntsListener.prototype.constructor = BugHuntsListener;

// Enter a parse tree produced by BugHuntsParser#items.
BugHuntsListener.prototype.enterItems = function(ctx) {
};

// Exit a parse tree produced by BugHuntsParser#items.
BugHuntsListener.prototype.exitItems = function(ctx) {
};


// Enter a parse tree produced by BugHuntsParser#ruleFO.
BugHuntsListener.prototype.enterRuleFO = function(ctx) {
};

// Exit a parse tree produced by BugHuntsParser#ruleFO.
BugHuntsListener.prototype.exitRuleFO = function(ctx) {
};


// Enter a parse tree produced by BugHuntsParser#ruleBA.
BugHuntsListener.prototype.enterRuleBA = function(ctx) {
};

// Exit a parse tree produced by BugHuntsParser#ruleBA.
BugHuntsListener.prototype.exitRuleBA = function(ctx) {
};


// Enter a parse tree produced by BugHuntsParser#ruleRI.
BugHuntsListener.prototype.enterRuleRI = function(ctx) {
};

// Exit a parse tree produced by BugHuntsParser#ruleRI.
BugHuntsListener.prototype.exitRuleRI = function(ctx) {
};


// Enter a parse tree produced by BugHuntsParser#ruleLE.
BugHuntsListener.prototype.enterRuleLE = function(ctx) {
};

// Exit a parse tree produced by BugHuntsParser#ruleLE.
BugHuntsListener.prototype.exitRuleLE = function(ctx) {
};



exports.BugHuntsListener = BugHuntsListener;